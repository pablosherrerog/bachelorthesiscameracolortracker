## Video
UC3M Computer Engineering Bachelor's Thesis: Computer visión integrated with virtual reality devices applied to video games

Author: Pablo Sánchez-Herrero Gómez
Tutor: Yago Saez Achaerandio
https://www.youtube.com/watch?v=pCz_zCBTLQw
## How to run it?
Open the project with visual studio 2015 (not tested with 2013 or 2017) and run it in release mode
It was tested with Kinect camera V1 for Windows and a laptop camera (without depth)

## How it works?
It tracks two colors, by default red and green but you can change it with the mouse
It sends the coordinantes of the colored markers using UDP.
The camera feed can be kinect or the webcam camera (by default webcam camera). To change it, change `bool standardCamera = true;` to `false` in `Main.cpp`

Later with another program you can catch the markers position (e.g using unity https://www.youtube.com/watch?v=aE4_VtUDORw)
In my case I did something similar in Unity to move a lightsaber 

If the webcam is used it will send 2D coordinates (since no depth can be provided), if the kinect camera is selected it will send 3D coordinates.

To extract the marker HSV values use the left click in the mouse, drag to make a rectangle. Use the right click in the mouse to change the marker 1 to 2 and vice versa

Press c to stop/reload de video feedback (better performance), press e to close the program

