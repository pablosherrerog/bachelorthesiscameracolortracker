//color tracker

//Written by  Pablo S�nchez-Herrero G�mez in 2016

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software")
//, to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

//------------------------------------//
//The main goal of this program is to track two color markers and send the coordinates with UDP. This can be done with a kinect camera v1 for windows   
//or with a regular camera (see standarCameraFunction) the option of using a regular camera is enabled by default but if you wanna use Kinect you can change the bool standardCamera to false).
//The kinect camera gets the 3d coordinates and a more accurate position by using the distance of the tracked object.
//I used OpenNI to get the data from the Kinect camera but if you are going to use the standarCameraFunction you can get rid of it.

// You will need to configure the project to use the OpenCV library (I used the 3.1 version) and the OpenNI2 library if you wanna use the Kinect camera
//(to use the kinect camera in the computer you will need to download also the SDK and the Developer ToolKit)

// I posted in the description of the video (https://www.youtube.com/watch?v=pCz_zCBTLQw) more info about useful resources that I used
// credits to Kyle Hounslow, some of the methods are based in his code https://raw.githubusercontent.com/kylehounslow/opencv-tuts/master/auto-colour-filter/AutoColourFilter.cpp
//------------------------------------//
#define _WINSOCK_DEPRECATED_NO_WARNINGS // with this we can use inet_addr (sendData)
#include <opencv2\opencv.hpp>
#include <OpenNI.h> 
#include <conio.h>
#include <iostream>
#include <winsock2.h>
//#include <stdio.h>
// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")
using namespace openni;
using namespace std;
using namespace cv;

VideoStream depthSensor;
VideoStream colorSensor;
int window_width = 640;
int window_height = 480;
//blue default values
int H_MIN = 0;
int H_MAX = 14;
int S_MIN = 135;
int S_MAX = 253;
int V_MIN = 85;
int V_MAX = 213;
//green default values
int H_MIN2 = 28;
int H_MAX2 = 85;
int S_MIN2 = 49;
int S_MAX2 = 253;
int V_MIN2 = 65;
int V_MAX2 = 149;

const Scalar green = Scalar(0, 255, 0);
const Scalar blue_2 = Scalar(0, 255, 255);
const Scalar yellow = Scalar(255, 255, 0);
int minArea = 40;

int grabFirstColor = true;

char* windowName1 = "Color";
char* windowName2 = "HSVMerged";
Mat depthcv(Size(window_width, window_height), CV_16UC1);
Mat colorMat(Size(window_width, window_height), CV_8UC3);

bool leftClick = false;
int xClick = -1;
int yClick = -1;
float wZAnterior = 1000, wZ2Anterior = 1000;
float wXAnterior = 0;
float wYAnterior = 0;
float wX2Anterior = 2;
float wY2Anterior = 2;
bool pause = false;
bool show_video = true;
Mat depthcv_paused;
Mat colorMat_paused;


string intToString(int number) {
	stringstream ss;
	ss << number;
	return ss.str();
}
int sendData(int xPos, int yPos, int zPos, int x2Pos, int y2Pos, int z2Pos) {
	//extracted from https://msdn.microsoft.com/en-us/library/windows/desktop/ms740148(v=vs.85).aspx
	int iResult;
	WSADATA wsaData;

	SOCKET SendSocket = INVALID_SOCKET;
	sockaddr_in RecvAddr;

	unsigned short Port = 5005;
	string messageString = intToString(xPos) + "/" + intToString(yPos) + "/" + intToString(zPos) + "/"
		+ intToString(x2Pos) + "/" + intToString(y2Pos) + "/" + intToString(z2Pos);
	const char *message = messageString.c_str();

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		wprintf(L"WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	//---------------------------------------------
	// Create a socket for sending data
	SendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (SendSocket == INVALID_SOCKET) {
		wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Set up the RecvAddr structure with the IP address of
	// the receiver (in this example case "192.168.1.1")
	// and the specified port number.
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(Port);
	RecvAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	//---------------------------------------------
	// Send a datagram to the receiver
	//wprintf(L"Sending a datagram to the receiver...\n");
	iResult = sendto(SendSocket,
		message, strlen(message), 0, (SOCKADDR *)& RecvAddr, sizeof(RecvAddr));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		closesocket(SendSocket);
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// When the application is finished sending, close the socket.
	//wprintf(L"Finished sending. Closing socket.\n");
	iResult = closesocket(SendSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Clean up and quit.
	//wprintf(L"Exiting.\n");
	WSACleanup();
	//cout << message << endl;
	return 0;
}

void drawObject(int x, int y, Mat &frame) {
	int squareDimension = 30;
	Point center = Point(x, y);
	Point corner1 = Point(x - squareDimension, y + squareDimension);
	Point corner2 = Point(x + squareDimension, y + squareDimension);
	Point corner3 = Point(x + squareDimension, y - squareDimension);
	Point corner4 = Point(x - squareDimension, y - squareDimension);
	//see how to create more in http://docs.opencv.org/2.4/doc/tutorials/core/basic_geometric_drawing/basic_geometric_drawing.html
	circle(frame, center, 10, green, 2);
	rectangle(frame, corner2, corner4, green, 2, 8);
	line(frame, corner1, corner3, green, 2, 8);
	line(frame, corner2, corner4, green, 2, 8);
	putText(frame, intToString(x) + "," + intToString(y), Point(x + squareDimension, y + squareDimension + 15), 1, 1, green, 2);
}
bool trackObject(int &x, int &y, Mat binaryImage, Mat &cameraFeed) {
	int maxArea = 0;
	int maxCont = 0;
	bool objectFound = false;
	Mat binaryImageTemp;
	binaryImage.copyTo(binaryImageTemp);
	vector< vector<Point> > contours;
	findContours(binaryImageTemp, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	for (int i = 0; i < contours.size(); i++) {
		Moments moment = moments(contours[i]);
		double area = moment.m00;
		if (area > minArea&&area > maxArea) {
			x = moment.m10 / area;
			y = moment.m01 / area;
			objectFound = true;
			maxArea = area;
			maxCont = i;
		}
	}
	if (objectFound == true) {
		drawObject(x, y, cameraFeed);
		drawContours(cameraFeed, contours, maxCont, blue_2, 2);
	}
	return objectFound;
}
void applyFilter(Mat &binaryImage)
{
	Mat element(3, 3, CV_8U, cv::Scalar(1));
	erode(binaryImage, binaryImage, element, Point(-1, -1), 2);
	dilate(binaryImage, binaryImage, element, Point(-1, -1), 5);
}
bool mouseIsDragging;//used for showing a rectangle on screen as user clicks and drags mouse
bool mouseMove;
bool rectangleSelected;
cv::Point initialClickPoint, currentMousePoint; //keep track of initial point clicked and current position of mouse
cv::Rect rectangleROI; //this is the ROI that the user has selected
vector<int> H_ROI, S_ROI, V_ROI;// HSV values from the click/drag ROI region stored in separate vectors so that we can sort them easily
vector<int> H_ROI2, S_ROI2, V_ROI2;// HSV values from the click/drag ROI region stored in separate vectors so that we can sort them easily
void clickAndDrag_Rectangle(int event, int x, int y, int flags, void* param) {
	//only if calibration mode is true will we use the mouse to change HSV values

	//get handle to video feed passed in as "param" and cast as Mat pointer
	Mat* videoFeed = (Mat*)param;

	if (event == CV_EVENT_LBUTTONDOWN && mouseIsDragging == false)
	{
		//keep track of initial point clicked
		initialClickPoint = cv::Point(x, y);
		//user has begun dragging the mouse
		mouseIsDragging = true;
	}
	//user is dragging the mouse 

	if (event == CV_EVENT_MOUSEMOVE && mouseIsDragging == true)
	{
		//keep track of current mouse point
		currentMousePoint = cv::Point(x, y);
		//user has moved the mouse while clicking and dragging
		mouseMove = true;
	}
	// user has released left button 
	if (event == CV_EVENT_LBUTTONUP && mouseIsDragging == true)
	{
		//set rectangle ROI to the rectangle that the user has selected
		rectangleROI = Rect(initialClickPoint, currentMousePoint);

		//reset boolean variables
		mouseIsDragging = false;
		mouseMove = false;
		rectangleSelected = true;
	}

	if (event == CV_EVENT_RBUTTONDOWN) {
		grabFirstColor = !grabFirstColor;
	}
}
void recordHSV_Values(cv::Mat frame, cv::Mat hsv_frame) {
	if (mouseMove == false && rectangleSelected == true) {
		//clear previous vector values
		if (H_ROI.size() > 0) H_ROI.clear();
		if (S_ROI.size() > 0) S_ROI.clear();
		if (V_ROI.size() > 0)V_ROI.clear();
		//if the rectangle has no width or height (user has only dragged a line) then we don't try to iterate over the width or height
		if (rectangleROI.width < 1 || rectangleROI.height < 1) cout << "Please drag a rectangle, not a line" << endl;
		else {
			for (int i = rectangleROI.x; i < rectangleROI.x + rectangleROI.width; i++) {
				//iterate through both x and y direction and save HSV values at each and every point
				for (int j = rectangleROI.y; j < rectangleROI.y + rectangleROI.height; j++) {
					//save HSV value at this point
					// [0] if for the [h] from HSV, [1]->S [2]->V

					H_ROI.push_back((int)hsv_frame.at<cv::Vec3b>(j, i)[0]);
					S_ROI.push_back((int)hsv_frame.at<cv::Vec3b>(j, i)[1]);
					V_ROI.push_back((int)hsv_frame.at<cv::Vec3b>(j, i)[2]);
				}
			}
		}
		//reset rectangleSelected so user can select another region if necessary
		rectangleSelected = false;
		//set min and max HSV values from min and max elements of each array
		if (grabFirstColor) {
			if (H_ROI.size() > 0) {
				//NOTE: min_element and max_element return iterators so we must dereference them with "*"
				H_MIN = *std::min_element(H_ROI.begin(), H_ROI.end());
				H_MAX = *std::max_element(H_ROI.begin(), H_ROI.end());
				cout << "MIN 'H' VALUE: " << H_MIN << endl;
				cout << "MAX 'H' VALUE: " << H_MAX << endl;
			}
			if (S_ROI.size() > 0) {
				S_MIN = *std::min_element(S_ROI.begin(), S_ROI.end());
				S_MAX = *std::max_element(S_ROI.begin(), S_ROI.end());
				cout << "MIN 'S' VALUE: " << S_MIN << endl;
				cout << "MAX 'S' VALUE: " << S_MAX << endl;
			}
			if (V_ROI.size() > 0) {
				V_MIN = *std::min_element(V_ROI.begin(), V_ROI.end());
				V_MAX = *std::max_element(V_ROI.begin(), V_ROI.end());
				cout << "MIN 'V' VALUE: " << V_MIN << endl;
				cout << "MAX 'V' VALUE: " << V_MAX << endl;
			}
		}
		else {
			if (H_ROI.size() > 0) {
				//NOTE: min_element and max_element return iterators so we must dereference them with "*"
				H_MIN2 = *std::min_element(H_ROI.begin(), H_ROI.end());
				H_MAX2 = *std::max_element(H_ROI.begin(), H_ROI.end());
				cout << "MIN2 'H' VALUE: " << H_MIN2 << endl;
				cout << "MAX2 'H' VALUE: " << H_MAX2 << endl;
			}
			if (S_ROI.size() > 0) {
				S_MIN2 = *std::min_element(S_ROI.begin(), S_ROI.end());
				S_MAX2 = *std::max_element(S_ROI.begin(), S_ROI.end());
				cout << "MIN2 'S' VALUE: " << S_MIN2 << endl;
				cout << "MAX2 'S' VALUE: " << S_MAX2 << endl;
			}
			if (V_ROI.size() > 0) {
				V_MIN2 = *std::min_element(V_ROI.begin(), V_ROI.end());
				V_MAX2 = *std::max_element(V_ROI.begin(), V_ROI.end());
				cout << "MIN2 'V' VALUE: " << V_MIN2 << endl;
				cout << "MAX2 'V' VALUE: " << V_MAX2 << endl;
			}
		}

	}
	if (mouseMove == true) {
		//if the mouse is held down, we will draw the click and dragged rectangle to the screen
		rectangle(frame, initialClickPoint, cv::Point(currentMousePoint.x, currentMousePoint.y), yellow, 1, 8, 0);
	}
}
// the coordinates are outside the function because if a tracked object is not found it will use the past value
float wXObj1 = 0, wYObj1 = 0, wZObj1 = 1000;
float wXObj2 = 0, wYObj2 = 0, wZObj2 = 1000;
int counter = 0;
int mainLoop()
{
	Mat cameraFeed;
	Mat HSV;
	Mat binaryImage;
	Mat binaryImage2;
	int xObj1 = 0;
	int yObj1 = 0;
	int xObj2 = 0;
	int yObj2 = 0;
	if (depthSensor.isValid() && colorSensor.isValid())
	{
		VideoStream* streamPointer = &depthSensor;
		int streamReadyIndex;
		OpenNI::waitForAnyStream(&streamPointer, 1, &streamReadyIndex, 500);
		if (streamReadyIndex == 0) {
			VideoFrameRef depthFrame;
			VideoFrameRef colorFrame;
			depthSensor.readFrame(&depthFrame);
			colorSensor.readFrame(&colorFrame);
			depthcv.data = (uchar*)depthFrame.getData();
			colorMat.data = (uchar*)colorFrame.getData();
			cvtColor(colorMat, colorMat, CV_BGR2RGB);
			cvtColor(colorMat, HSV, COLOR_RGB2HSV);
			recordHSV_Values(colorMat, HSV);
			// we search the elements in the range of hsv min and hsv max of the color matrix 
			// (having transformed before from BGR to HSV(by default opencv use BGR format))
			// This will give us an array of 1 and 0 where 1 represent the elements from the color we want to track
			inRange(HSV, Scalar(H_MIN, S_MIN, V_MIN), Scalar(H_MAX, S_MAX, V_MAX), binaryImage);
			inRange(HSV, Scalar(H_MIN2, S_MIN2, V_MIN2), Scalar(H_MAX2, S_MAX2, V_MAX2), binaryImage2);
			Mat binaryImageMerged;
			addWeighted(binaryImage, 1.0, binaryImage2, 1.0, 0.0, binaryImageMerged);
			applyFilter(binaryImageMerged);
			applyFilter(binaryImage);
			applyFilter(binaryImage2);
			//extracted from openni cookbook
			if (trackObject(xObj1, yObj1, binaryImage, colorMat)) {
				DepthPixel* pixel1 =
					(DepthPixel*)((char*)depthFrame.getData() +
					(yObj1  *
						depthFrame.getStrideInBytes()))
					+ (xObj1);

				CoordinateConverter::convertDepthToWorld(
					depthSensor,
					(float)(xObj1),
					(float)(yObj1),
					(float)(*pixel1),
					&wXObj1, &wYObj1, &wZObj1);
			}
			if (trackObject(xObj2, yObj2, binaryImage2, colorMat)) {
				DepthPixel* pixel2 =
					(DepthPixel*)((char*)depthFrame.getData() +
					(yObj2  *
						depthFrame.getStrideInBytes()))
					+ (xObj2);

				CoordinateConverter::convertDepthToWorld(
					depthSensor,
					(float)(xObj2),
					(float)(yObj2),
					(float)(*pixel2),
					&wXObj2, &wYObj2, &wZObj2);
			}
			// we apply a filter to minize possible jumps from the z coordinate
			if (abs(wZAnterior - wZObj1) > 1000) {
				wZObj1 = wZAnterior;
			}
			if (abs(wZ2Anterior - wZObj2) > 1000) {
				wZObj2 = wZ2Anterior;
			}
			if (wZObj1 > 800 && wZObj2 > 800) {
				wZAnterior = wZObj1;
				wZ2Anterior = wZObj2;
				sendData(wXObj1, wYObj1, wZObj1, wXObj2, wYObj2, wZObj2);
				wXAnterior = wXObj1;
				wYAnterior = wYObj1;
				wX2Anterior = wXObj2;
				wY2Anterior = wYObj2;
			}
			// thanks to this we can extract the character without pausing the program
			if (_kbhit()) {
				char k;
				k = _getch();
				cout << k << endl;
				switch (k) {
				case 'e': return 1;  break;
				case 'c': show_video = !show_video; break;
				}
			}
			if (show_video) {
				imshow(windowName1, colorMat);
				imshow(windowName2, binaryImageMerged);
				waitKey(1);
			}
		}
	}
	return 0;
}
// Used only by the standarCameraFunction this functions are really simple they put negative values to the bot and left coordinates, 
//the new center of the screen is the 0,0 position.
float pixelToMMFormatX(float coordinate){
	return (coordinate - window_width / 2);
	}
float pixelToMMFormatY(float coordinate) {
	return -coordinate + window_height / 2;
}
void standarCameraFunction() {
	Mat HSV;
	Mat binaryImage;
	Mat binaryImage2;
	VideoCapture capture;
	int xObj1 = 0;
	int yObj1 = 0;
	int xObj2 = 0;
	int yObj2 = 0;
	//open capture object at location zero (default location for webcam)
	capture.open(0);
	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, window_width);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, window_height);
	while (true) {
		capture.read(colorMat);
		flip(colorMat, colorMat, 1);
		//in this case my camera returns me RGB data, if the color looks weird you need to change COLOR_RGB2HSV to COLOR_BGR2HSV 
		cvtColor(colorMat, HSV, COLOR_RGB2HSV);
		cvtColor(colorMat, HSV, COLOR_RGB2HSV);
		recordHSV_Values(colorMat, HSV);
		inRange(HSV, Scalar(H_MIN, S_MIN, V_MIN), Scalar(H_MAX, S_MAX, V_MAX), binaryImage);
		inRange(HSV, Scalar(H_MIN2, S_MIN2, V_MIN2), Scalar(H_MAX2, S_MAX2, V_MAX2), binaryImage2);
		Mat binaryImageMerged;
		addWeighted(binaryImage, 1.0, binaryImage2, 1.0, 0.0, binaryImageMerged);
		applyFilter(binaryImageMerged);
		applyFilter(binaryImage);
		applyFilter(binaryImage2);
		if (trackObject(xObj1, yObj1, binaryImage, colorMat)) {
			wXObj1 = pixelToMMFormatX(xObj1);
			wYObj1 = pixelToMMFormatY(yObj1);
		}
		if (trackObject(xObj2, yObj2, binaryImage2, colorMat)) {
			wXObj2 = pixelToMMFormatX(xObj2);
			wYObj2 = pixelToMMFormatY(yObj2);
		}
		sendData(wXObj1, wYObj1, wZObj1, wXObj2, wYObj2, wZObj2);
		if (_kbhit()) {
			char k;
			k = _getch();
			cout << k << endl;
			switch (k) {
			case 'e': return;  break;
			case 'c': show_video = !show_video; break;
			}
		}
		if (show_video) {
			//in this case my camera returns me RGB data, if the color looks weird you will need to discoment this line
			//	cvtColor(colorMat, colorMat, CV_BGR2RGB);
			imshow(windowName1, colorMat);
			imshow(windowName2, binaryImageMerged);
			waitKey(1);
		}
	}
}
int main() {
	// modify this value to alternate between the kinect camera or the webcam 
	// (webcam will use the default location, if there are several cameras connected it can give problems)
	bool standardCamera = true;
	namedWindow(windowName1);
	namedWindow(windowName2);
	moveWindow(windowName1, 0, 0);
	HWND consoleWindow = GetConsoleWindow();
	cv::setMouseCallback(windowName1, clickAndDrag_Rectangle, &colorMat);
	SetWindowPos(consoleWindow, 0, 640, 510, 640, 515, SWP_NOZORDER);
	moveWindow(windowName2, 0, 510);
	printf("Press c to stop/reload de video feedback (better performance), press e to close the program \n");
	printf("To extract the marker HSV values use the left click in the mouse, drag to make a rectangle. Use the right click in the mouse to change the marker 1 to 2 and vice versa  \n");
	if (standardCamera) {
		standarCameraFunction();
		return 0;
	}
	OpenNI::initialize();
	Device device;

	if (device.open(ANY_DEVICE) == 1) {
		printf("Problem opening the Kinect camera, press enter to exit \n");
		// this is used to assure that the user can read the message
		char c;
		c = getchar();
		device.close();
		OpenNI::shutdown();
		return 0;
	}
	printf("Name of the device %s \n", device.getDeviceInfo().getName());
	printf("Creating depth stream\n");
	depthSensor.create(device, SENSOR_DEPTH);
	printf("Depth stream, setting video mode\n");
	VideoMode vmod;
	vmod.setFps(30);
	vmod.setPixelFormat(PIXEL_FORMAT_DEPTH_1_MM);
	vmod.setResolution(window_height, window_width);
	depthSensor.setVideoMode(vmod);
	printf("Starting depth stream\n");
	depthSensor.start();
	printf("Creating color stream\n");
	colorSensor.create(device, SENSOR_COLOR);
	printf("Color stream, setting video mode\n");
	vmod.setFps(30);
	vmod.setPixelFormat(PIXEL_FORMAT_RGB888);
	vmod.setResolution(window_height, window_width);
	colorSensor.setVideoMode(vmod);
	printf("Starting color stream\n");
	colorSensor.start();
	//printf("Enabling Depth-Image frames sync\n");
	device.setDepthColorSyncEnabled(true);
	// this allow us to align the color data with the depth data (because the cameras arent in the same position) 
	printf("Enabling Depth to Image mapping\n");
	device.setImageRegistrationMode(IMAGE_REGISTRATION_DEPTH_TO_COLOR);

	while (true) {
		if (mainLoop() == 1) {
			break;
		}
	}
	depthSensor.destroy();
	colorSensor.destroy();
	device.close();
	OpenNI::shutdown();
	return 0;
}
